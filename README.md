CDLae
============

After Effects plug-in to apply a CDL.


#####Written by

[Brendan Bolles](http://gitlab.com/fnordware/)



#####License

[BSD](http://opensource.org/licenses/BSD-2-Clause)