ext
===

You will need to manually add the the After Effects 2021 SDK, which you can get from here:

[After Effects SDK](http://www.adobe.com/devnet/aftereffects.html)
