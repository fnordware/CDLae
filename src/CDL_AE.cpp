///////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2024, Brendan Bolles
// 
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
// *	   Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
// *	   Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
///////////////////////////////////////////////////////////////////////////

// ------------------------------------------------------------------------
//
// CDL_AE.cpp
//
// After Effects plug-in to apply a CDL
//
// ------------------------------------------------------------------------


#include "CDL_AE.h"

#include "AEGP_SuiteHandler.h"

#include <float.h>
#include <assert.h>


static PF_Err 
About(	
	PF_InData		*in_data,
	PF_OutData		*out_data,
	PF_ParamDef		*params[],
	PF_LayerDef		*output )
{
	PF_SPRINTF( out_data->return_msg, 
				"%s - %s\r\rwritten by %s\r\rv%d.%d - %s\r\r%s\r%s",
				NAME,
				DESCRIPTION,
				AUTHOR, 
				MAJOR_VERSION, 
				MINOR_VERSION,
				RELEASE_DATE,
				COPYRIGHT,
				WEBSITE);
				
	return PF_Err_NONE;
}


static PF_Err 
GlobalSetup (	
	PF_InData		*in_data,
	PF_OutData		*out_data,
	PF_ParamDef		*params[],
	PF_LayerDef		*output )
{
	out_data->my_version	=	PF_VERSION( MAJOR_VERSION, 
											MINOR_VERSION,
											BUG_VERSION, 
											STAGE_VERSION, 
											BUILD_VERSION);

	out_data->out_flags		=	PF_OutFlag_DEEP_COLOR_AWARE		|
								PF_OutFlag_PIX_INDEPENDENT		|
								PF_OutFlag_USE_OUTPUT_EXTENT;

	out_data->out_flags2	=	PF_OutFlag2_PARAM_GROUP_START_COLLAPSED_FLAG |
								PF_OutFlag2_SUPPORTS_SMART_RENDER	|
								PF_OutFlag2_FLOAT_COLOR_AWARE	|
								PF_OutFlag2_SUPPORTS_GPU_RENDER_F32 |
								PF_OutFlag2_SUPPORTS_THREADED_RENDERING;
	
	if(in_data->appl_id == 'PrMr')
	{
		PF_PixelFormatSuite1 *pfS = NULL;
		
		in_data->pica_basicP->AcquireSuite(kPFPixelFormatSuite,
											kPFPixelFormatSuiteVersion1,
											(const void **)&pfS);
											
		if(pfS)
		{
			pfS->ClearSupportedPixelFormats(in_data->effect_ref);
			
			pfS->AddSupportedPixelFormat(in_data->effect_ref, PrPixelFormat_BGRA_4444_8u);
			pfS->AddSupportedPixelFormat(in_data->effect_ref, PrPixelFormat_BGRA_4444_16u);
			pfS->AddSupportedPixelFormat(in_data->effect_ref, PrPixelFormat_BGRA_4444_32f);
			
			in_data->pica_basicP->ReleaseSuite(kPFPixelFormatSuite,
												kPFPixelFormatSuiteVersion1);
		}

		out_data->out_flags2 ^= PF_OutFlag2_SUPPORTS_GPU_RENDER_F32; // Premiere not handling GPU well
	}
	
	return PF_Err_NONE;
}


static PF_Err
ParamsSetup(
	PF_InData		*in_data,
	PF_OutData		*out_data,
	PF_ParamDef		*params[],
	PF_LayerDef		*output)
{
#define ADD_SLOPE_SLIDER(NAME, ID) \
	AEFX_CLR_STRUCT(def); \
	PF_ADD_FLOAT_SLIDER(NAME, -FLT_MAX, FLT_MAX, 0.f, 5.f, 0.f, 1.f, 2, 0, 0, ID);

#define ADD_OFFSET_SLIDER(NAME, ID) \
	AEFX_CLR_STRUCT(def); \
	PF_ADD_FLOAT_SLIDER(NAME, -FLT_MAX, FLT_MAX, -1.f, 1.f, 0.f, 0.f, 2, 0, 0, ID);

#define ADD_POWER_SLIDER(NAME, ID) \
	AEFX_CLR_STRUCT(def); \
	PF_ADD_FLOAT_SLIDER(NAME, FLT_MIN, FLT_MAX, FLT_MIN, 5.f, 0.f, 1.f, 2, 0, 0, ID);

#define ADD_SATURATION_SLIDER(NAME, ID) \
	AEFX_CLR_STRUCT(def); \
	PF_ADD_FLOAT_SLIDER(NAME, 0.f, FLT_MAX, 0.f, 2.f, 0.f, 1.f, 2, 0, 0, ID);

#define ADD_TOPIC(NAME, ID) \
	AEFX_CLR_STRUCT(def); \
	PF_ADD_TOPIC(NAME, ID)

#define END_TOPIC(ID) \
	AEFX_CLR_STRUCT(def); \
	PF_END_TOPIC(ID)


	PF_Err err = PF_Err_NONE;
	
	PF_ParamDef def;
	
	ADD_TOPIC("Slope", CDL_SLOPE_TOPIC_BEGIN_ID);
	ADD_SLOPE_SLIDER("Red Slope", CDL_RED_SLOPE_ID);
	ADD_SLOPE_SLIDER("Green Slope", CDL_GREEN_SLOPE_ID);
	ADD_SLOPE_SLIDER("Blue Slope", CDL_BLUE_SLOPE_ID);
	END_TOPIC(CDL_SLOPE_TOPIC_END_ID);
	
	ADD_TOPIC("Offset", CDL_OFFSET_TOPIC_BEGIN_ID);
	ADD_OFFSET_SLIDER("Red Offset", CDL_RED_OFFSET_ID);
	ADD_OFFSET_SLIDER("Green Offset", CDL_GREEN_OFFSET_ID);
	ADD_OFFSET_SLIDER("Blue Offset", CDL_BLUE_OFFSET_ID);
	END_TOPIC(CDL_OFFSET_TOPIC_END_ID);
	
	ADD_TOPIC("Power", CDL_POWER_TOPIC_BEGIN_ID);
	ADD_POWER_SLIDER("Red Power", CDL_RED_POWER_ID);
	ADD_POWER_SLIDER("Green Power", CDL_GREEN_POWER_ID);
	ADD_POWER_SLIDER("Blue Power", CDL_BLUE_POWER_ID);
	END_TOPIC(CDL_POWER_TOPIC_END_ID);
	
	ADD_TOPIC("Sat", CDL_SATURATION_TOPIC_BEGIN_ID);
	ADD_SATURATION_SLIDER("Saturation", CDL_SATURATION_ID);
	END_TOPIC(CDL_SATURATION_TOPIC_END_ID);
	
	PF_ADD_CHECKBOX("", "Inverse transform", FALSE, 0, CDL_INVERSE_ID);
	
	
	out_data->num_params = CDL_NUM_PARAMS;
	
	return err;
}


static PF_Boolean IsEmptyRect(const PF_LRect *r){
	return (r->left >= r->right) || (r->top >= r->bottom);
}

#ifndef mmin
	#define mmin(a,b) ((a) < (b) ? (a) : (b))
	#define mmax(a,b) ((a) > (b) ? (a) : (b))
#endif

static void UnionLRect(const PF_LRect *src, PF_LRect *dst)
{
	if (IsEmptyRect(dst)) {
		*dst = *src;
	} else if (!IsEmptyRect(src)) {
		dst->left	= mmin(dst->left, src->left);
		dst->top	= mmin(dst->top, src->top);
		dst->right	= mmax(dst->right, src->right);
		dst->bottom = mmax(dst->bottom, src->bottom);
	}
}

static PF_Err PreRender(
	PF_InData				*in_data,
	PF_OutData				*out_data,
	PF_PreRenderExtra		*extra)
{
	PF_Err err = PF_Err_NONE;
	PF_RenderRequest req = extra->input->output_request;
	PF_CheckoutResult in_result;
	
	req.preserve_rgb_of_zero_alpha = TRUE;

	ERR(extra->cb->checkout_layer(	in_data->effect_ref,
									CDL_INPUT,
									CDL_INPUT,
									&req,
									in_data->current_time,
									in_data->time_step,
									in_data->time_scale,
									&in_result));


	UnionLRect(&in_result.result_rect,		&extra->output->result_rect);
	UnionLRect(&in_result.max_result_rect,	&extra->output->max_result_rect);

	if(extra->input->what_gpu == PF_GPU_Framework_OPENCL)
	{
		extra->output->flags |= PF_RenderOutputFlag_GPU_RENDER_POSSIBLE;
	}
#ifdef HAVE_METAL
	else if(extra->input->what_gpu == PF_GPU_Framework_METAL)
	{
		extra->output->flags |= PF_RenderOutputFlag_GPU_RENDER_POSSIBLE;
	}
#endif
#ifdef HAVE_CUDA
	else if(extra->input->what_gpu == PF_GPU_Framework_CUDA)
	{
		extra->output->flags |= PF_RenderOutputFlag_GPU_RENDER_POSSIBLE;
	}
#endif

	return err;
}


template <typename CHAN_TYPE>
static inline float ConvertToFloat(CHAN_TYPE in);

template <>
static inline float ConvertToFloat(A_u_char in)
{
	return (float)in / (float)PF_MAX_CHAN8;
}

template <>
static inline float ConvertToFloat(A_u_short in)
{
	return (float)in / (float)PF_MAX_CHAN16;
}

template <>
static inline float ConvertToFloat(PF_FpShort in)
{
	return in;
}


static inline float Clamp(float in)
{
	return (in >= 1.f ? 1.f : in <= 0.f ? 0.f : in);
}


template <typename CHAN_TYPE>
static inline CHAN_TYPE ConvertToAE(float in);

template <>
static inline A_u_char ConvertToAE<A_u_char>(float in)
{
	return ( Clamp(in) * (float)PF_MAX_CHAN8 ) + 0.5f;
}

template <>
static inline A_u_short ConvertToAE<A_u_short>(float in)
{
	return ( Clamp(in) * (float)PF_MAX_CHAN16 ) + 0.5f;
}

template <>
static inline PF_FpShort ConvertToAE<PF_FpShort>(float in)
{
	return in;
}


static inline float Power(float in, float exp)
{
	return (in < 0.f ? in : powf(in, exp));
}

template <typename T>
struct PremierePixel {
	T blue;
	T green;
	T red;
	T alpha;
};


typedef struct {
	A_long	width;
	float	red_slope;
	float	green_slope;
	float	blue_slope;
	float	red_offset;
	float	green_offset;
	float	blue_offset;
	float	red_power;
	float	green_power;
	float	blue_power;
	float	saturation;
	bool	inverse;
} ProcessData;

template <typename AE_PIXTYPE, typename WP_PIXTYPE, typename CHAN_TYPE>
static PF_Err
ProcessRow(
	void			*refcon, 
	A_long			x, 
	A_long			y, 
	AE_PIXTYPE		*inP, 
	AE_PIXTYPE		*outP)
{
	ProcessData *p_data = (ProcessData *)refcon;
	
	WP_PIXTYPE *in = (WP_PIXTYPE *)inP;
	WP_PIXTYPE *out = (WP_PIXTYPE *)outP;
	
	for(int x=0; x < p_data->width; x++)
	{
		PF_PixelFloat inpix;
		
		inpix.red = ConvertToFloat( in->red );
		inpix.green = ConvertToFloat( in->green );
		inpix.blue = ConvertToFloat( in->blue );
		
		
		PF_PixelFloat outpix;
		
		if(p_data->inverse)
		{
			if(p_data->saturation != 1.f && p_data->saturation != 0.f)
			{
				// Rec. 709 coefficients
				static const float lumaWeights[3] = { 0.2126f, 0.7152f, 0.0722f };
				
				const float luma = (lumaWeights[0] * inpix.red) +
									(lumaWeights[1] * inpix.green) +
									(lumaWeights[2] * inpix.blue);
				
				const float inv_saturation = (1.f / p_data->saturation);
				
				inpix.red = luma + inv_saturation * (inpix.red - luma);
				inpix.green = luma + inv_saturation * (inpix.green - luma);
				inpix.blue = luma + inv_saturation * (inpix.blue - luma);
			}
			
			outpix.red = (Power(inpix.red, 1.f / (p_data->red_power == 0.f ? 1.f : p_data->red_power)) - p_data->red_offset) / (p_data->red_slope == 0.f ? 1.f : p_data->red_slope);
			outpix.green = (Power(inpix.green, 1.f / (p_data->green_power == 0.f ? 1.f : p_data->green_power)) - p_data->green_offset) / (p_data->green_slope == 0.f ? 1.f : p_data->green_slope);
			outpix.blue = (Power(inpix.blue, 1.f / (p_data->blue_power == 0.f ? 1.f : p_data->blue_power)) - p_data->blue_offset) / (p_data->blue_slope == 0.f ? 1.f : p_data->blue_slope);
		}
		else
		{
			outpix.red = Power((inpix.red * p_data->red_slope) + p_data->red_offset, p_data->red_power);
			outpix.green = Power((inpix.green * p_data->green_slope) + p_data->green_offset, p_data->green_power);
			outpix.blue = Power((inpix.blue * p_data->blue_slope) + p_data->blue_offset, p_data->blue_power);
			
			if(p_data->saturation != 1.f)
			{
				// https://github.com/AcademySoftwareFoundation/OpenColorIO/blob/main/src/OpenColorIO/ops/cdl/CDLOpCPU.cpp#L199
				static const float lumaWeights[3] = { 0.2126f, 0.7152f, 0.0722f };
				
				const float luma = (lumaWeights[0] * outpix.red) +
									(lumaWeights[1] * outpix.green) +
									(lumaWeights[2] * outpix.blue);
				
				outpix.red = luma + p_data->saturation * (outpix.red - luma);
				outpix.green = luma + p_data->saturation * (outpix.green - luma);
				outpix.blue = luma + p_data->saturation * (outpix.blue - luma);
			}
		}

		out->red   = ConvertToAE<CHAN_TYPE>( outpix.red );
		out->green = ConvertToAE<CHAN_TYPE>( outpix.green );
		out->blue  = ConvertToAE<CHAN_TYPE>( outpix.blue );
		
		out->alpha = in->alpha;
		
		in++;
		out++;
	}

	return PF_Err_NONE;
}



static PF_Err DoRender(
	PF_InData		*in_data,
	PF_EffectWorld	*input,
	PF_ParamDef		*CDL_red_slope,
	PF_ParamDef		*CDL_green_slope,
	PF_ParamDef		*CDL_blue_slope,
	PF_ParamDef		*CDL_red_offset,
	PF_ParamDef		*CDL_green_offset,
	PF_ParamDef		*CDL_blue_offset,
	PF_ParamDef		*CDL_red_power,
	PF_ParamDef		*CDL_green_power,
	PF_ParamDef		*CDL_blue_power,
	PF_ParamDef		*CDL_saturation,
	PF_ParamDef		*CDL_inverse,
	PF_OutData		*out_data,
	PF_EffectWorld	*output)
{
	PF_Err				err		= PF_Err_NONE;

	AEGP_SuiteHandler suites(in_data->pica_basicP);
	
	PF_WorldSuite2 *wsP = NULL;
	ERR( in_data->pica_basicP->AcquireSuite(kPFWorldSuite, kPFWorldSuiteVersion2, (const void **)&wsP) );
	
	
	// Get the pixel format
	PF_PixelFormat format = PF_PixelFormat_INVALID;			
	ERR( wsP->PF_GetPixelFormat(output, &format) );
	
	
	// Premiere pixel info
	if(in_data->appl_id == 'PrMr')
	{
		PF_PixelFormatSuite1 *pfS = NULL;
		in_data->pica_basicP->AcquireSuite(kPFPixelFormatSuite, kPFPixelFormatSuiteVersion1, (const void **)&pfS);
		
		if(pfS != NULL)
		{
			// the regular world suite function will give a bogus value for Premiere
			ERR( pfS->GetPixelFormat(output, (PrPixelFormat *)&format) );
			
			in_data->pica_basicP->ReleaseSuite(kPFPixelFormatSuite, kPFPixelFormatSuiteVersion1);
		}
	}
	
	
	if(!err)
	{
		try
		{
			PF_Point			origin;
			PF_Rect				areaR;
			
			origin.h = in_data->output_origin_x;
			origin.v = in_data->output_origin_y;

			areaR.left		= 0;
			areaR.right		= 1;
			
			areaR.top		= 0;
			areaR.bottom	= output->height;
			
			
			ProcessData p_data;
			
			p_data.width = output->width;
			p_data.red_slope = CDL_red_slope->u.fs_d.value;
			p_data.green_slope = CDL_green_slope->u.fs_d.value;
			p_data.blue_slope = CDL_blue_slope->u.fs_d.value;
			p_data.red_offset = CDL_red_offset->u.fs_d.value;
			p_data.green_offset = CDL_green_offset->u.fs_d.value;
			p_data.blue_offset = CDL_blue_offset->u.fs_d.value;
			p_data.red_power = CDL_red_power->u.fs_d.value;
			p_data.green_power = CDL_green_power->u.fs_d.value;
			p_data.blue_power = CDL_blue_power->u.fs_d.value;
			p_data.saturation = CDL_saturation->u.fs_d.value;
			p_data.inverse = CDL_inverse->u.bd.value;
			
		
			if(format == PF_PixelFormat_ARGB32)
			{
				err = suites.Iterate8Suite1()->iterate_origin(in_data,
																0,
																output->height,
																input,
																&areaR,
																&origin,
																&p_data,
																ProcessRow<PF_Pixel, PF_Pixel, A_u_char>,
																output);
			}
			else if(format == PF_PixelFormat_ARGB64)
			{
				err = suites.Iterate16Suite1()->iterate_origin(in_data,
																0,
																output->height,
																input,
																&areaR,
																&origin,
																&p_data,
																ProcessRow<PF_Pixel16, PF_Pixel16, A_u_short>,
																output);
			}
			else if(format == PF_PixelFormat_ARGB128)
			{
				err = suites.IterateFloatSuite1()->iterate_origin(in_data,
																0,
																output->height,
																input,
																&areaR,
																&origin,
																&p_data,
																ProcessRow<PF_Pixel32, PF_Pixel32, PF_FpShort>,
																output);
			}
			else
			{
				assert(in_data->appl_id == 'PrMr');

				PF_Iterate8Suite1 *it8P = NULL;
				in_data->pica_basicP->AcquireSuite(kPFIterate8Suite, kPFIterate8SuiteVersion1, (const void **)&it8P);

				if(it8P != NULL && it8P->iterate_origin != NULL)
				{
					if(format == PrPixelFormat_BGRA_4444_8u)
					{
						err = it8P->iterate_origin(in_data,
													0,
													output->height,
													input,
													&areaR,
													&origin,
													&p_data,
													ProcessRow<PF_Pixel, PremierePixel<A_u_char>, A_u_char>,
													output);
					}
					else if(format == PrPixelFormat_BGRA_4444_16u)
					{
						err = it8P->iterate_origin(in_data,
													0,
													output->height,
													input,
													&areaR,
													&origin,
													&p_data,
													ProcessRow<PF_Pixel, PremierePixel<A_u_short>, A_u_short>,
													output);
					}
					else if(format == PrPixelFormat_BGRA_4444_32f)
					{
						err = it8P->iterate_origin(in_data,
													0,
													output->height,
													input,
													&areaR,
													&origin,
													&p_data,
													ProcessRow<PF_Pixel, PremierePixel<PF_FpShort>, PF_FpShort>,
													output);
					}
					else
						assert(FALSE);

					in_data->pica_basicP->ReleaseSuite(kPFIterate8Suite, kPFIterate8SuiteVersion1);
				}
				else
				{
					// Oh COME ON, Premiere!

				#ifdef NDEBUG
					#define PROG(CURRENT, TOTAL)	((err = PF_PROGRESS(in_data, CURRENT, TOTAL)) == PF_Err_NONE)
				#else
					#define PROG(CURRENT, TOTAL)	TRUE
				#endif

					for(int y=0; y < output->height && !err && PROG(y, output->height); y++)
					{
						PF_Pixel *in = (PF_Pixel *)((char *)input->data + (y * input->rowbytes));
						PF_Pixel *out = (PF_Pixel *)((char *)output->data + (y * output->rowbytes));

						if(format == PrPixelFormat_BGRA_4444_8u)
							err = ProcessRow<PF_Pixel, PremierePixel<A_u_char>, A_u_char>(&p_data, 0, y, in, out);
						else if(format == PrPixelFormat_BGRA_4444_16u)
							err = ProcessRow<PF_Pixel, PremierePixel<A_u_short>, A_u_short>(&p_data, 0, y, in, out);
						else if(format == PrPixelFormat_BGRA_4444_32f)
							err = ProcessRow<PF_Pixel, PremierePixel<PF_FpShort>, PF_FpShort>(&p_data, 0, y, in, out);
						else
							assert(FALSE);
					}
				}
			}
		}
		catch(...) { err = PF_Err_INTERNAL_STRUCT_DAMAGED; }
	}
	
	
	if(wsP)
		in_data->pica_basicP->ReleaseSuite(kPFWorldSuite, kPFWorldSuiteVersion2);
		

	return err;
}


static PF_Err
SmartRender(
	PF_InData				*in_data,
	PF_OutData				*out_data,
	PF_SmartRenderExtra		*extra,
	bool					gpu)
{
	PF_Err			err		= PF_Err_NONE,
					err2	= PF_Err_NONE;
					
	PF_EffectWorld *input, *output;
	
	PF_ParamDef CDL_red_slope,
				CDL_green_slope,
				CDL_blue_slope,
				CDL_red_offset,
				CDL_green_offset,
				CDL_blue_offset,
				CDL_red_power,
				CDL_green_power,
				CDL_blue_power,
				CDL_saturation,
				CDL_inverse;

	// zero-out parameters
	AEFX_CLR_STRUCT(CDL_red_slope);
	AEFX_CLR_STRUCT(CDL_green_slope);
	AEFX_CLR_STRUCT(CDL_blue_slope);
	AEFX_CLR_STRUCT(CDL_red_offset);
	AEFX_CLR_STRUCT(CDL_green_offset);
	AEFX_CLR_STRUCT(CDL_blue_offset);
	AEFX_CLR_STRUCT(CDL_red_power);
	AEFX_CLR_STRUCT(CDL_green_power);
	AEFX_CLR_STRUCT(CDL_blue_power);
	AEFX_CLR_STRUCT(CDL_saturation);
	AEFX_CLR_STRUCT(CDL_inverse);
	

	// checkout input & output buffers.
	ERR(	extra->cb->checkout_layer_pixels( in_data->effect_ref, CDL_INPUT, &input)	);
	ERR(	extra->cb->checkout_output( in_data->effect_ref, &output)	);


	// bail before param checkout
	if(err)
		return err;

#define PF_CHECKOUT_PARAM_NOW( PARAM, DEST )	PF_CHECKOUT_PARAM(	in_data, (PARAM), in_data->current_time, in_data->time_step, in_data->time_scale, DEST )

	// checkout the required params
	ERR(	PF_CHECKOUT_PARAM_NOW( RED_SLOPE,		&CDL_red_slope )	);
	ERR(	PF_CHECKOUT_PARAM_NOW( GREEN_SLOPE,		&CDL_green_slope )	);
	ERR(	PF_CHECKOUT_PARAM_NOW( BLUE_SLOPE,		&CDL_blue_slope )	);
	ERR(	PF_CHECKOUT_PARAM_NOW( RED_OFFSET,		&CDL_red_offset )	);
	ERR(	PF_CHECKOUT_PARAM_NOW( GREEN_OFFSET,	&CDL_green_offset )	);
	ERR(	PF_CHECKOUT_PARAM_NOW( BLUE_OFFSET,		&CDL_blue_offset )	);
	ERR(	PF_CHECKOUT_PARAM_NOW( RED_POWER,		&CDL_red_power )	);
	ERR(	PF_CHECKOUT_PARAM_NOW( GREEN_POWER,		&CDL_green_power )	);
	ERR(	PF_CHECKOUT_PARAM_NOW( BLUE_POWER,		&CDL_blue_power )	);
	ERR(	PF_CHECKOUT_PARAM_NOW( SATURATION,		&CDL_saturation )	);
	ERR(	PF_CHECKOUT_PARAM_NOW( INVERSE,			&CDL_inverse )	);

	const PF_GPU_Framework what_gpu = (gpu ? extra->input->what_gpu : PF_GPU_Framework_NONE);

	if(what_gpu == PF_GPU_Framework_OPENCL)
	{
		ERR(GPURender_OpenCL(in_data,
							input,
							&CDL_red_slope,
							&CDL_green_slope,
							&CDL_blue_slope,
							&CDL_red_offset,
							&CDL_green_offset,
							&CDL_blue_offset,
							&CDL_red_power,
							&CDL_green_power,
							&CDL_blue_power,
							&CDL_saturation,
							&CDL_inverse,
							out_data,
							output,
							extra) );		}
#ifdef HAVE_METAL
	else if(what_gpu == PF_GPU_Framework_METAL)
	{
		ERR(GPURender_Metal(in_data,
							input,
							&CDL_red_slope,
							&CDL_green_slope,
							&CDL_blue_slope,
							&CDL_red_offset,
							&CDL_green_offset,
							&CDL_blue_offset,
							&CDL_red_power,
							&CDL_green_power,
							&CDL_blue_power,
							&CDL_saturation,
							&CDL_inverse,
							out_data,
							output,
							extra) );
	}
#endif
#ifdef HAVE_CUDA
	else if(what_gpu == PF_GPU_Framework_CUDA)
	{
		ERR(GPURender_CUDA(in_data,
							input,
							&CDL_red_slope,
							&CDL_green_slope,
							&CDL_blue_slope,
							&CDL_red_offset,
							&CDL_green_offset,
							&CDL_blue_offset,
							&CDL_red_power,
							&CDL_green_power,
							&CDL_blue_power,
							&CDL_saturation,
							&CDL_inverse,
							out_data,
							output,
							extra));
	}
#endif
	else
	{
		ERR(DoRender(	in_data,
						input,
						&CDL_red_slope,
						&CDL_green_slope,
						&CDL_blue_slope,
						&CDL_red_offset,
						&CDL_green_offset,
						&CDL_blue_offset,
						&CDL_red_power,
						&CDL_green_power,
						&CDL_blue_power,
						&CDL_saturation,
						&CDL_inverse,
						out_data,
						output) );
	}

	// Always check in, no matter what the error condition!
	ERR2(	PF_CHECKIN_PARAM(in_data, &CDL_red_slope )	);
	ERR2(	PF_CHECKIN_PARAM(in_data, &CDL_green_slope )	);
	ERR2(	PF_CHECKIN_PARAM(in_data, &CDL_blue_slope )	);
	ERR2(	PF_CHECKIN_PARAM(in_data, &CDL_red_offset )	);
	ERR2(	PF_CHECKIN_PARAM(in_data, &CDL_green_offset ) );
	ERR2(	PF_CHECKIN_PARAM(in_data, &CDL_blue_offset )	);
	ERR2(	PF_CHECKIN_PARAM(in_data, &CDL_red_power )	);
	ERR2(	PF_CHECKIN_PARAM(in_data, &CDL_green_power )	);
	ERR2(	PF_CHECKIN_PARAM(in_data, &CDL_blue_power )	);
	ERR2(	PF_CHECKIN_PARAM(in_data, &CDL_saturation )	);
	ERR2(	PF_CHECKIN_PARAM(in_data, &CDL_inverse )	);


	return err;
}


static PF_Err Render(
	PF_InData		*in_data,
	PF_OutData		*out_data,
	PF_ParamDef		*params[],
	PF_LayerDef		*output )
{
	return DoRender(in_data,
					&params[CDL_INPUT]->u.ld,
					params[RED_SLOPE],
					params[GREEN_SLOPE],
					params[BLUE_SLOPE],
					params[RED_OFFSET],
					params[GREEN_OFFSET],
					params[BLUE_OFFSET],
					params[RED_POWER],
					params[GREEN_POWER],
					params[BLUE_POWER],
					params[SATURATION],
					params[INVERSE],
					out_data,
					output);
}


static PF_Err
GPUDeviceSetup(
	PF_InData	*in_data,
	PF_OutData	*out_data,
	PF_GPUDeviceSetupExtra	*extra)
{
	if(extra->input->what_gpu == PF_GPU_Framework_OPENCL)
	{
		return GPUDeviceSetup_OpenCL(in_data, out_data, extra);
	}
#ifdef HAVE_METAL
	else if(extra->input->what_gpu == PF_GPU_Framework_METAL)
	{
		return GPUDeviceSetup_Metal(in_data, out_data, extra);
	}
#endif
#ifdef HAVE_CUDA
	else if(extra->input->what_gpu == PF_GPU_Framework_CUDA)
	{
		return GPUDeviceSetup_CUDA(in_data, out_data, extra);
	}
#endif
	else
		return PF_Err_NONE;
}

PF_Err
GPUDeviceSetdown(
	PF_InData	*in_data,
	PF_OutData	*out_data,
	PF_GPUDeviceSetdownExtra	*extra)
{
	if(extra->input->what_gpu == PF_GPU_Framework_OPENCL)
	{
		return GPUDeviceSetdown_OpenCL(in_data, out_data, extra);
	}
#ifdef HAVE_METAL
	else if(extra->input->what_gpu == PF_GPU_Framework_METAL)
	{
		return GPUDeviceSetdown_Metal(in_data, out_data, extra);
	}
#endif
#ifdef HAVE_CUDA
	else if(extra->input->what_gpu == PF_GPU_Framework_CUDA)
	{
		return GPUDeviceSetdown_CUDA(in_data, out_data, extra);
	}
#endif
	else
		return PF_Err_NONE;
}


DllExport	
PF_Err 
PluginMain (	
	PF_Cmd			cmd,
	PF_InData		*in_data,
	PF_OutData		*out_data,
	PF_ParamDef		*params[],
	PF_LayerDef		*output,
	void			*extra)
{
	PF_Err err = PF_Err_NONE;
	
	try {
		switch (cmd) {
			case PF_Cmd_ABOUT:
				err = About(in_data, out_data, params, output);
				break;
			case PF_Cmd_GLOBAL_SETUP:
				err = GlobalSetup(in_data, out_data, params, output);
				break;
			case PF_Cmd_PARAMS_SETUP:
				err = ParamsSetup(in_data, out_data, params, output);
				break;
			case PF_Cmd_SMART_PRE_RENDER:
				err = PreRender(in_data, out_data, (PF_PreRenderExtra*)extra);
				break;
			case PF_Cmd_SMART_RENDER:
				err = SmartRender(in_data, out_data, (PF_SmartRenderExtra*)extra, false);
				break;
			case PF_Cmd_RENDER:
				err = Render(in_data, out_data, params, output);
				break;
			case PF_Cmd_GPU_DEVICE_SETUP:
				err = GPUDeviceSetup(in_data, out_data, (PF_GPUDeviceSetupExtra*)extra);
				break;
			case PF_Cmd_GPU_DEVICE_SETDOWN:
				err = GPUDeviceSetdown(in_data, out_data, (PF_GPUDeviceSetdownExtra*)extra);
				break;
			case PF_Cmd_SMART_RENDER_GPU:
				err = SmartRender(in_data, out_data, (PF_SmartRenderExtra*)extra, true);
				break;
		}
	}
	catch(PF_Err &thrown_err) { err = thrown_err; }
	catch(...) { err = PF_Err_INTERNAL_STRUCT_DAMAGED; }
	
	return err;
}

