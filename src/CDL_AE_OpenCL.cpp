///////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2024, Brendan Bolles
// 
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
// *	   Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
// *	   Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
///////////////////////////////////////////////////////////////////////////

// ------------------------------------------------------------------------
//
// CDL_AE_OpenCL.cpp
//
// After Effects plug-in to apply a CDL
//
// ------------------------------------------------------------------------


#include "CDL_AE.h"


#include <AE_EffectSuites.h>
#include <AE_EffectCBSuites.h>
#include <AE_EffectGPUSuites.h>

#include "AEFX_SuiteHelper.h"

#ifndef NDEBUG
#include <stdio.h>
#endif

#include <assert.h>

#ifdef MAC_ENV
#include <OpenCL/cl.h>
#else
#include <CL/cl.h>
#endif

static const char *cdlFunctionName = "CDLKernel";

static const char *cdlShader = R"fnord(

static __inline__ uint2 KernelXYUnsigned()
{
	return (uint2)(get_global_id(0), get_global_id(1));
}

static __inline__ float Power(
	float in,
	float exp)
{
	return in < 0.f ? in : pow(in, exp);
}

static __inline__ float CDL(
	float in,
	float slope,
	float offset,
	float power)
{
	return Power((in * slope) + offset, power);
}

static __inline__ float InverseCDL(
	float in,
	float slope,
	float offset,
	float power)
{
	return (Power(in, 1.f / (power == 0.f ? 1.f : power)) - offset) / (slope == 0.f ? 1.f : slope);
}

static __inline__ float4 Saturation(
	float4 in,
	float sat)
{
	float luma = (in.z * 0.2126f) + (in.y * 0.7152f) + (in.x * 0.0722f);
	
	float4 out;
	
	out.x = luma + sat * (in.x - luma);
	out.y = luma + sat * (in.y - luma);
	out.z = luma + sat * (in.z - luma);
	out.w = in.w;
	
	return out;
}

__kernel void CDLKernel(
	const __global float4 *input,
	__global float4 *output,
	int				inputPitch,
	int				outputPitch,
	unsigned int	width,
	unsigned int	height,
	float			red_slope,
	float			green_slope,
	float			blue_slope,
	float			red_offset,
	float			green_offset,
	float			blue_offset,
	float			red_power,
	float			green_power,
	float			blue_power,
	float			saturation,
	unsigned int	inverse)
{
	uint2 pos = KernelXYUnsigned();
	
	if(pos.x < width && pos.y < height)
	{
		float4 pixel = input[(pos.y * inputPitch) + pos.x];
		
		if(inverse)
		{
			if(saturation != 1.f && saturation != 0.f)
			{
				pixel = Saturation(pixel, 1.f / saturation);
			}
			
			pixel.x = InverseCDL(pixel.x, blue_slope, blue_offset, blue_power);
			pixel.y = InverseCDL(pixel.y, green_slope, green_offset, green_power);
			pixel.z = InverseCDL(pixel.z, red_slope, red_offset, red_power);
		}
		else
		{
			pixel.x = CDL(pixel.x, blue_slope, blue_offset, blue_power);
			pixel.y = CDL(pixel.y, green_slope, green_offset, green_power);
			pixel.z = CDL(pixel.z, red_slope, red_offset, red_power);
			
			if(saturation != 1.f)
			{
				pixel = Saturation(pixel, saturation);
			}
		}
		
		output[(pos.y * outputPitch) + pos.x] = pixel;
	}
}

)fnord";


typedef struct
{
	cl_kernel cdl_kernel;
} OpenCLGPUData;


static size_t
RoundUp(
	size_t inValue,
	size_t inMultiple)
{
	return inValue ? ((inValue + inMultiple - 1) / inMultiple) * inMultiple : 0;
}


PF_Err
GPUDeviceSetup_OpenCL(
	PF_InData	*in_data,
	PF_OutData	*out_data,
	PF_GPUDeviceSetupExtra	*extra)
{
	PF_Err err = PF_Err_NONE;
	
	AEFX_SuiteScoper<PF_HandleSuite1> handleSuite(in_data, kPFHandleSuite, kPFHandleSuiteVersion1, out_data);
	AEFX_SuiteScoper<PF_GPUDeviceSuite1> gpuDeviceSuite(in_data, kPFGPUDeviceSuite, kPFGPUDeviceSuiteVersion1, out_data);
	
	PF_GPUDeviceInfo device_info;
	gpuDeviceSuite->GetDeviceInfo(in_data->effect_ref, extra->input->device_index, &device_info);
	
	char const *strings[] = { cdlShader };
	size_t sizes[] = { strlen(cdlShader) };

	cl_context context = (cl_context)device_info.contextPV;
	cl_device_id device = (cl_device_id)device_info.devicePV;
	
	cl_int result = CL_SUCCESS;
	
	cl_program program = clCreateProgramWithSource(context, 1, strings, sizes, &result);
	
	if(result == CL_SUCCESS)
	{
		result = clBuildProgram(program, 1, &device, "-cl-single-precision-constant -cl-fast-relaxed-math", NULL, NULL);
		
		if(result == CL_SUCCESS)
		{
			PF_Handle opencl_handle = handleSuite->host_new_handle(sizeof(OpenCLGPUData));
			OpenCLGPUData *opencl_data = (OpenCLGPUData *)handleSuite->host_lock_handle(opencl_handle);
			
			opencl_data->cdl_kernel = clCreateKernel(program, cdlFunctionName, &result);
			
			if(result == CL_SUCCESS)
			{
				extra->output->gpu_data = opencl_handle;
				
				out_data->out_flags2 |= PF_OutFlag2_SUPPORTS_GPU_RENDER_F32;
			}
			else
				err = PF_Err_INTERNAL_STRUCT_DAMAGED;
			
			handleSuite->host_unlock_handle(opencl_handle);
		}
		else
		{
		#ifndef NDEBUG
			size_t build_log_len = 0;
			
			cl_int infoResult = clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, 0, NULL, &build_log_len);
			
			char *log = (char *)malloc(build_log_len);
			
			infoResult = clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, build_log_len, log, NULL);
			
			printf(log);
			
			free(log);
		#endif
		
			err = PF_Err_INTERNAL_STRUCT_DAMAGED;
		}
	}
	else
		err = PF_Err_INTERNAL_STRUCT_DAMAGED;

	return err;
}


PF_Err
GPUDeviceSetdown_OpenCL(
	PF_InData	*in_data,
	PF_OutData	*out_data,
	PF_GPUDeviceSetdownExtra	*extra)
{
	PF_Err err = PF_Err_NONE;
	
	if(extra->input->gpu_data != NULL)
	{
		AEFX_SuiteScoper<PF_HandleSuite1> handleSuite(in_data, kPFHandleSuite, kPFHandleSuiteVersion1, out_data);

		OpenCLGPUData *opencl_data = (OpenCLGPUData *)handleSuite->host_lock_handle((PF_Handle)extra->input->gpu_data);

		clReleaseKernel(opencl_data->cdl_kernel);
		
		handleSuite->host_dispose_handle((PF_Handle)extra->input->gpu_data);
	}
	else
		assert(FALSE);
	
	return err;
}


PF_Err
GPURender_OpenCL(
	PF_InData		*in_data,
	PF_EffectWorld	*input,
	PF_ParamDef		*CDL_red_slope,
	PF_ParamDef		*CDL_green_slope,
	PF_ParamDef		*CDL_blue_slope,
	PF_ParamDef		*CDL_red_offset,
	PF_ParamDef		*CDL_green_offset,
	PF_ParamDef		*CDL_blue_offset,
	PF_ParamDef		*CDL_red_power,
	PF_ParamDef		*CDL_green_power,
	PF_ParamDef		*CDL_blue_power,
	PF_ParamDef		*CDL_saturation,
	PF_ParamDef		*CDL_inverse,
	PF_OutData		*out_data,
	PF_EffectWorld	*output,
	PF_SmartRenderExtra	*extra)
{
	PF_Err err = PF_Err_NONE;
	
#ifndef NDEBUG
	AEFX_SuiteScoper<PF_WorldSuite2> worldSuite(in_data, kPFWorldSuite, kPFWorldSuiteVersion2, out_data);
	
	PF_PixelFormat inputFormat, outputFormat;
	worldSuite->PF_GetPixelFormat(input, &inputFormat);
	worldSuite->PF_GetPixelFormat(output, &outputFormat);
	assert(inputFormat == PF_PixelFormat_GPU_BGRA128);
	assert(outputFormat == PF_PixelFormat_GPU_BGRA128);
	
	assert(input->width == output->width);
	assert(input->height == output->height);
#endif
	
	AEFX_SuiteScoper<PF_HandleSuite1> handleSuite(in_data, kPFHandleSuite, kPFHandleSuiteVersion1, out_data);
	AEFX_SuiteScoper<PF_GPUDeviceSuite1> gpuDeviceSuite(in_data, kPFGPUDeviceSuite, kPFGPUDeviceSuiteVersion1, out_data);
	
	PF_GPUDeviceInfo device_info;
	gpuDeviceSuite->GetDeviceInfo(in_data->effect_ref, extra->input->device_index, &device_info);
	
	void *input_mem = NULL;
	gpuDeviceSuite->GetGPUWorldData(in_data->effect_ref, input, &input_mem);
	
	void *output_mem = NULL;
	gpuDeviceSuite->GetGPUWorldData(in_data->effect_ref, output, &output_mem);
	
	OpenCLGPUData *opencl_data = (OpenCLGPUData *)handleSuite->host_lock_handle((PF_Handle)extra->input->gpu_data);
	
	
	const size_t pixelSize = 4 * sizeof(float);
	
	const int inputPitch = input->rowbytes / pixelSize;
	const int outputPitch = output->rowbytes / pixelSize;
	const unsigned int width = output->width;
	const unsigned int height = output->height;
	const float red_slope = CDL_red_slope->u.fs_d.value;
	const float green_slope = CDL_green_slope->u.fs_d.value;
	const float blue_slope = CDL_blue_slope->u.fs_d.value;
	const float red_offset = CDL_red_offset->u.fs_d.value;
	const float green_offset = CDL_green_offset->u.fs_d.value;
	const float blue_offset = CDL_blue_offset->u.fs_d.value;
	const float red_power = CDL_red_power->u.fs_d.value;
	const float green_power = CDL_green_power->u.fs_d.value;
	const float blue_power = CDL_blue_power->u.fs_d.value;
	const float saturation = CDL_saturation->u.fs_d.value;
	const unsigned int inverse = CDL_inverse->u.bd.value;
	
	cl_mem cl_input_mem = (cl_mem)input_mem;
	cl_mem cl_output_mem = (cl_mem)output_mem;
	
	cl_uint idx = 0;
	
	clSetKernelArg(opencl_data->cdl_kernel, idx++, sizeof(cl_mem), &cl_input_mem);
	clSetKernelArg(opencl_data->cdl_kernel, idx++, sizeof(cl_mem), &cl_output_mem);
	clSetKernelArg(opencl_data->cdl_kernel, idx++, sizeof(int), &inputPitch);
	clSetKernelArg(opencl_data->cdl_kernel, idx++, sizeof(int), &outputPitch);
	clSetKernelArg(opencl_data->cdl_kernel, idx++, sizeof(unsigned int), &width);
	clSetKernelArg(opencl_data->cdl_kernel, idx++, sizeof(unsigned int), &height);
	clSetKernelArg(opencl_data->cdl_kernel, idx++, sizeof(float), &red_slope);
	clSetKernelArg(opencl_data->cdl_kernel, idx++, sizeof(float), &green_slope);
	clSetKernelArg(opencl_data->cdl_kernel, idx++, sizeof(float), &blue_slope);
	clSetKernelArg(opencl_data->cdl_kernel, idx++, sizeof(float), &red_offset);
	clSetKernelArg(opencl_data->cdl_kernel, idx++, sizeof(float), &green_offset);
	clSetKernelArg(opencl_data->cdl_kernel, idx++, sizeof(float), &blue_offset);
	clSetKernelArg(opencl_data->cdl_kernel, idx++, sizeof(float), &red_power);
	clSetKernelArg(opencl_data->cdl_kernel, idx++, sizeof(float), &green_power);
	clSetKernelArg(opencl_data->cdl_kernel, idx++, sizeof(float), &blue_power);
	clSetKernelArg(opencl_data->cdl_kernel, idx++, sizeof(float), &saturation);
	clSetKernelArg(opencl_data->cdl_kernel, idx++, sizeof(unsigned int), &inverse);
	
	const size_t threadBlock[2] = { 16, 16 };
	const size_t grid[2] = { RoundUp(width, threadBlock[0]), RoundUp(height, threadBlock[1])};

	cl_int result = clEnqueueNDRangeKernel((cl_command_queue)device_info.command_queuePV,
											opencl_data->cdl_kernel,
											2,
											NULL,
											grid,
											threadBlock,
											0,
											NULL,
											NULL);
	
	if(result != CL_SUCCESS)
		err = PF_Err_INTERNAL_STRUCT_DAMAGED;

	handleSuite->host_unlock_handle((PF_Handle)extra->input->gpu_data);
	
	return err;
}
