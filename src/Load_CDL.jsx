
// Load_CDL.jsx
// by Brendan Bolles
//
// This script will read .cdl, .cc, .ccc, possibly other files and apply CDL effects
// to a selected layer in After Effects.
//
// Warranty: None Whatsoever
// License: Public Domain

(function(thisObj){

function GetColorCorrections(xmlObj)
{
	var name = xmlObj.name().localName;

	if(name == "ColorCorrection" || name == "ASC_CDL")
	{
		return [xmlObj];
	}
	else
	{
		var result = [];
	
		for(var i=0; i < xmlObj.elements().length(); i++)
		{
			var element = xmlObj.child(i);
			
			var elementCCs = GetColorCorrections(element);
			
			for(var j=0; j < elementCCs.length; j++)
				result.push(elementCCs[j]);
		}
		
		return result;
	}
}

function ApplyCDLFileToLayer(cdlFile, layer)
{
	try
	{
		var opened = cdlFile.open("r");
	
		if(!opened)
		{
			alert("Failed to open CDL");
			return;
		}
	
		var cdlText = cdlFile.read();
	
		cdlFile.close();
	
		var cdlXML = new XML(cdlText);
	
		var colorCorrections = GetColorCorrections(cdlXML);
	
		if(colorCorrections.length == 0)
		{
			alert("Invalid CDL file");
			return;
		}
	
		app.beginUndoGroup("Load CDL");
	
		for(var i=0; i < colorCorrections.length; i++)
		{
			var colorCorrection = colorCorrections[i];
		
			var sopNode = null;
			var satNode = null;
		
			for(var j=0; j < colorCorrection.elements().length(); j++)
			{
				var node = colorCorrection.child(j);
			
				if(node.name().localName == "SOPNode")
					sopNode = node;
				else if(node.name().localName == "SatNode")
					satNode = node;
			}
		
			if(sopNode != null || satNode != null)
			{
				var cdlEffect = layer.effect.addProperty("fnord CDL");
			
				cdlEffect.name = cdlFile.displayName;
			
				if(colorCorrection.@id != undefined && colorCorrection.@id.toString().length > 0)
				{
					cdlEffect.name += " " + colorCorrection.@id.toString();
				}
				else if(colorCorrections.length > 1)
				{
					cdlEffect.name += " " + (i + 1);
				}
			
				if(sopNode != null)
				{
					function RGBtriple(txt)
					{
						var regex = /([0-9\-\.e]+)\s+([0-9\-\.e]+)\s+([0-9\-\.e]+)/i;
					
						if(txt.search(regex) >= 0)
						{
							var rgb = [parseFloat(txt.replace(regex, "$1")), parseFloat(txt.replace(regex, "$2")), parseFloat(txt.replace(regex, "$3"))];
						
							if(isNaN(rgb[0]) || isNaN(rgb[1]) || isNaN(rgb[2]))
								return null;
							else
								return rgb;
						}
						else
							return null;
					}
				
					for(var j=0; j < sopNode.elements().length(); j++)
					{	
						var sop = sopNode.child(j);
					
						var sopName = sop.name().localName;
					
						if(sopName == "Slope" || sopName == "Offset" || sopName == "Power")
						{
							var rgb = RGBtriple(sop.text().toString());
						
							if(rgb instanceof Array)
							{
								if(sopName == "Slope")
								{
									cdlEffect.property("Red Slope").setValue(rgb[0]);
									cdlEffect.property("Green Slope").setValue(rgb[1]);
									cdlEffect.property("Blue Slope").setValue(rgb[2]);
								}
								else if(sopName == "Offset")
								{
									cdlEffect.property("Red Offset").setValue(rgb[0]);
									cdlEffect.property("Green Offset").setValue(rgb[1]);
									cdlEffect.property("Blue Offset").setValue(rgb[2]);
								}
								else // sopName == "Power"
								{
									cdlEffect.property("Red Power").setValue(rgb[0]);
									cdlEffect.property("Green Power").setValue(rgb[1]);
									cdlEffect.property("Blue Power").setValue(rgb[2]);
								}
							}
							else
								debugger;
						}
					}
				}
			
				if(satNode != null)
				{
					for(var j=0; j < satNode.elements().length(); j++)
					{
						var sat = satNode.child(j);
					
						var satName = sat.name().localName;
					
						if(satName == "Saturation")
						{
							var val = parseFloat(sat.text().toString());
						
							if(!isNaN(val))
								cdlEffect.property("Saturation").setValue(val);
							else
								debugger;
						}
					}
				}
			}
			else
				debugger;
		}
		
		app.endUndoGroup();
	}
	catch(e)
	{
		alert(e.toString());
	}
}

function LoadCDL()
{
	if(!(app.project.activeItem instanceof CompItem) || app.project.activeItem.selectedLayers.length != 1)
	{
		alert("Select a layer to apply a CDL to");
		return;
	}
	
	var layer = app.project.activeItem.selectedLayers[0];
	
	if(!(layer instanceof AVLayer || layer instanceof TextLayer || layer instanceof ShapeLayer))
	{
		alert("Select an effect-ready layer to apply a CDL to");
		return;
	}
	
	var cdlFile = File.openDialog("Choose a CDL");
	
	if(cdlFile != null)
	{
		ApplyCDLFileToLayer(cdlFile, layer);
	}
}

function buildUI(obj)
{
	var panel = (obj instanceof Panel) ? obj : new Window("palette", "Load CDL", undefined, {resizeable: false});
	
	var res =
	"group { \
		orientation: 'column', alignment: ['fill', 'fill'], \
		button: Button { text: 'Load CDL...', alignment: ['fill', 'top'] } \
	} \
	";
	
	var uiGroup = panel.add(res);
	
	uiGroup.button.onClick =
		function()
		{
			LoadCDL();
		}
	
	
	panel.layout.layout(true);
	panel.layout.resize();
	panel.onResizing = panel.onResize = function(){ panel.layout.resize(); };
	
	if(panel instanceof Window)
	{
		panel.center();
		panel.show();
	}
}

if(thisObj instanceof Panel)
	buildUI(thisObj);
else
	LoadCDL();

})(this);
