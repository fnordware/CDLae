///////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2024, Brendan Bolles
// 
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
// *       Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
// *       Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
///////////////////////////////////////////////////////////////////////////

// ------------------------------------------------------------------------
//
// CDL_AE.h
//
// After Effects plug-in to apply a CDL
//
// ------------------------------------------------------------------------


#ifndef INCLUDED_CDL_AE_H
#define INCLUDED_CDL_AE_H


#include "AEConfig.h"
#include "entry.h"
#include "SPTypes.h"
#include "PrSDKAESupport.h"
#include "AE_Macros.h"
#include "Param_Utils.h"
#include "AE_Effect.h"
#include "AE_EffectUI.h"
#include "AE_EffectCB.h"


#ifdef MSWindows
    #include <Windows.h>
#else 
    #ifndef __MACH__
        #include "string.h"
    #endif
#endif  


// Versioning information

#define NAME				"CDL"
#define DESCRIPTION			"Apply an ASC CDL"
#define RELEASE_DATE		__DATE__
#define AUTHOR				"by Brendan Bolles"
#define COPYRIGHT			"\xA9 2024 fnord"
#define WEBSITE				"www.fnordware.com"
#define	MAJOR_VERSION		1
#define	MINOR_VERSION		0
#define	BUG_VERSION			0
#define	STAGE_VERSION		PF_Stage_RELEASE
#define	BUILD_VERSION		0


// Paramater constants
enum {
    CDL_INPUT = 0,
    SLOPE_TOPIC_BEGIN,
	RED_SLOPE,
	GREEN_SLOPE,
	BLUE_SLOPE,
	SLOPE_TOPIC_END,
	OFFSET_TOPIC_BEGIN,
	RED_OFFSET,
	GREEN_OFFSET,
	BLUE_OFFSET,
	OFFSET_TOPIC_END,
	POWER_TOPIC_BEGIN,
	RED_POWER,
	GREEN_POWER,
	BLUE_POWER,
	POWER_TOPIC_END,
	SATURATION_TOPIC_BEGIN,
	SATURATION,
	SATURATION_TOPIC_END,
	INVERSE,
    
    CDL_NUM_PARAMS
};

enum {
	CDL_BUTTONS_ID = 1, // originally had some custom UI with Load/Save buttons
	CDL_RED_SLOPE_ID,
	CDL_GREEN_SLOPE_ID,
	CDL_BLUE_SLOPE_ID,
	CDL_RED_OFFSET_ID,
	CDL_GREEN_OFFSET_ID,
	CDL_BLUE_OFFSET_ID,
	CDL_RED_POWER_ID,
	CDL_GREEN_POWER_ID,
	CDL_BLUE_POWER_ID,
	CDL_SATURATION_ID,
	CDL_INVERSE_ID,
	
	CDL_SLOPE_TOPIC_BEGIN_ID,
	CDL_SLOPE_TOPIC_END_ID,
	CDL_OFFSET_TOPIC_BEGIN_ID,
	CDL_OFFSET_TOPIC_END_ID,
	CDL_POWER_TOPIC_BEGIN_ID,
	CDL_POWER_TOPIC_END_ID,
	CDL_SATURATION_TOPIC_BEGIN_ID,
	CDL_SATURATION_TOPIC_END_ID
};


#ifdef __cplusplus
	extern "C" {
#endif

DllExport	PF_Err 
PluginMain (	
	PF_Cmd			cmd,
	PF_InData		*in_data,
	PF_OutData		*out_data,
	PF_ParamDef		*params[],
	PF_LayerDef		*output,
	void			*extra) ;


PF_Err
GPUDeviceSetup_OpenCL(
	PF_InData	*in_data,
	PF_OutData	*out_data,
	PF_GPUDeviceSetupExtra	*extra);

PF_Err
GPUDeviceSetdown_OpenCL(
	PF_InData	*in_data,
	PF_OutData	*out_data,
	PF_GPUDeviceSetdownExtra	*extra);

PF_Err
GPURender_OpenCL(
	PF_InData		*in_data,
	PF_EffectWorld	*input,
	PF_ParamDef		*CDL_red_slope,
	PF_ParamDef		*CDL_green_slope,
	PF_ParamDef		*CDL_blue_slope,
	PF_ParamDef		*CDL_red_offset,
	PF_ParamDef		*CDL_green_offset,
	PF_ParamDef		*CDL_blue_offset,
	PF_ParamDef		*CDL_red_power,
	PF_ParamDef		*CDL_green_power,
	PF_ParamDef		*CDL_blue_power,
	PF_ParamDef		*CDL_saturation,
	PF_ParamDef		*CDL_inverse,
	PF_OutData		*out_data,
	PF_EffectWorld	*output,
	PF_SmartRenderExtra	*extra);


#ifdef MAC_ENV
	#define HAVE_METAL 1
#endif

#ifdef HAVE_METAL
PF_Err
GPUDeviceSetup_Metal(
	PF_InData	*in_data,
	PF_OutData	*out_data,
	PF_GPUDeviceSetupExtra	*extra);

PF_Err
GPUDeviceSetdown_Metal(
	PF_InData	*in_data,
	PF_OutData	*out_data,
	PF_GPUDeviceSetdownExtra	*extra);

PF_Err
GPURender_Metal(
	PF_InData		*in_data,
	PF_EffectWorld	*input,
	PF_ParamDef		*CDL_red_slope,
	PF_ParamDef		*CDL_green_slope,
	PF_ParamDef		*CDL_blue_slope,
	PF_ParamDef		*CDL_red_offset,
	PF_ParamDef		*CDL_green_offset,
	PF_ParamDef		*CDL_blue_offset,
	PF_ParamDef		*CDL_red_power,
	PF_ParamDef		*CDL_green_power,
	PF_ParamDef		*CDL_blue_power,
	PF_ParamDef		*CDL_saturation,
	PF_ParamDef		*CDL_inverse,
	PF_OutData		*out_data,
	PF_EffectWorld	*output,
	PF_SmartRenderExtra	*extra);
#endif // HAVE_METAL


#ifdef WIN_ENV
	#define HAVE_CUDA 1
#endif

#ifdef HAVE_CUDA
PF_Err
GPUDeviceSetup_CUDA(
	PF_InData	*in_data,
	PF_OutData	*out_data,
	PF_GPUDeviceSetupExtra	*extra);

PF_Err
GPUDeviceSetdown_CUDA(
	PF_InData	*in_data,
	PF_OutData	*out_data,
	PF_GPUDeviceSetdownExtra	*extra);

PF_Err
GPURender_CUDA(
	PF_InData		*in_data,
	PF_EffectWorld	*input,
	PF_ParamDef		*CDL_red_slope,
	PF_ParamDef		*CDL_green_slope,
	PF_ParamDef		*CDL_blue_slope,
	PF_ParamDef		*CDL_red_offset,
	PF_ParamDef		*CDL_green_offset,
	PF_ParamDef		*CDL_blue_offset,
	PF_ParamDef		*CDL_red_power,
	PF_ParamDef		*CDL_green_power,
	PF_ParamDef		*CDL_blue_power,
	PF_ParamDef		*CDL_saturation,
	PF_ParamDef		*CDL_inverse,
	PF_OutData		*out_data,
	PF_EffectWorld	*output,
	PF_SmartRenderExtra	*extra);
#endif // HAVE_CUDA

#ifdef __cplusplus
	}
#endif


#endif // INCLUDED_CDL_AE_H
