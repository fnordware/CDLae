///////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2024, Brendan Bolles
// 
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
// *	   Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
// *	   Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
///////////////////////////////////////////////////////////////////////////

// ------------------------------------------------------------------------
//
// CDL_AE_OpenCL.cpp
//
// After Effects plug-in to apply a CDL
//
// ------------------------------------------------------------------------


#include "CDL_AE.h"

#include <AE_EffectSuites.h>
#include <AE_EffectCBSuites.h>
#include <AE_EffectGPUSuites.h>

#include "AEFX_SuiteHelper.h"

#include <assert.h>

#undef MAJOR_VERSION
#undef MINOR_VERSION

#include <cuda_runtime.h>

#ifndef NDEBUG
#include <cuda.h>
#endif

extern void CDL_CUDA(
	float const		*input,
	float			*output,
	int				inputPitch,
	int				outputPitch,
	unsigned int	width,
	unsigned int	height,
	float			red_slope,
	float			green_slope,
	float			blue_slope,
	float			red_offset,
	float			green_offset,
	float			blue_offset,
	float			red_power,
	float			green_power,
	float			blue_power,
	float			saturation,
	bool			inverse);


PF_Err
GPUDeviceSetup_CUDA(
	PF_InData	*in_data,
	PF_OutData	*out_data,
	PF_GPUDeviceSetupExtra	*extra)
{
	int driverVersion = 0;
	cudaError_t cuErr = cudaDriverGetVersion(&driverVersion);

	if(cuErr == cudaSuccess && driverVersion >= CUDART_VERSION)
	{
#ifndef NDEBUG
		int runtimeVersion = 0;
		cudaRuntimeGetVersion(&runtimeVersion);

		assert(runtimeVersion == CUDART_VERSION);

		AEFX_SuiteScoper<PF_GPUDeviceSuite1> gpuDeviceSuite(in_data, kPFGPUDeviceSuite, kPFGPUDeviceSuiteVersion1, out_data);

		PF_GPUDeviceInfo device_info;
		gpuDeviceSuite->GetDeviceInfo(in_data->effect_ref, extra->input->device_index, &device_info);

		int deviceNum = -1;
		cudaGetDevice(&deviceNum);

		CUdevice device;
		cuDeviceGet(&device, deviceNum);

		assert(device == (CUdevice)device_info.devicePV);
#endif

		out_data->out_flags2 |= PF_OutFlag2_SUPPORTS_GPU_RENDER_F32;
	}

	return PF_Err_NONE;
}


PF_Err
GPUDeviceSetdown_CUDA(
	PF_InData	*in_data,
	PF_OutData	*out_data,
	PF_GPUDeviceSetdownExtra	*extra)
{
	return PF_Err_NONE;
}


PF_Err
GPURender_CUDA(
	PF_InData		*in_data,
	PF_EffectWorld	*input,
	PF_ParamDef		*CDL_red_slope,
	PF_ParamDef		*CDL_green_slope,
	PF_ParamDef		*CDL_blue_slope,
	PF_ParamDef		*CDL_red_offset,
	PF_ParamDef		*CDL_green_offset,
	PF_ParamDef		*CDL_blue_offset,
	PF_ParamDef		*CDL_red_power,
	PF_ParamDef		*CDL_green_power,
	PF_ParamDef		*CDL_blue_power,
	PF_ParamDef		*CDL_saturation,
	PF_ParamDef		*CDL_inverse,
	PF_OutData		*out_data,
	PF_EffectWorld	*output,
	PF_SmartRenderExtra	*extra)
{
	PF_Err err = PF_Err_NONE;
	
#ifndef NDEBUG
	AEFX_SuiteScoper<PF_WorldSuite2> worldSuite(in_data, kPFWorldSuite, kPFWorldSuiteVersion2, out_data);
	
	PF_PixelFormat inputFormat, outputFormat;
	worldSuite->PF_GetPixelFormat(input, &inputFormat);
	worldSuite->PF_GetPixelFormat(output, &outputFormat);
	assert(inputFormat == PF_PixelFormat_GPU_BGRA128);
	assert(outputFormat == PF_PixelFormat_GPU_BGRA128);
	
	assert(input->width == output->width);
	assert(input->height == output->height);
#endif
	
	AEFX_SuiteScoper<PF_GPUDeviceSuite1> gpuDeviceSuite(in_data, kPFGPUDeviceSuite, kPFGPUDeviceSuiteVersion1, out_data);

#ifndef NDEBUG
	PF_GPUDeviceInfo device_info;
	gpuDeviceSuite->GetDeviceInfo(in_data->effect_ref, extra->input->device_index, &device_info);

	int deviceNum = -1;
	cudaGetDevice(&deviceNum);

	CUdevice device;
	cuDeviceGet(&device, deviceNum);

	assert(device == (CUdevice)device_info.devicePV);

	CUcontext context;
	cuCtxGetCurrent(&context);

	assert(context == device_info.contextPV);
#endif

	void *input_mem = NULL;
	gpuDeviceSuite->GetGPUWorldData(in_data->effect_ref, input, &input_mem);
	
	void *output_mem = NULL;
	gpuDeviceSuite->GetGPUWorldData(in_data->effect_ref, output, &output_mem);
	
	const size_t pixelSize = 4 * sizeof(float);
	
	CDL_CUDA((float const *)input_mem,
				(float *)output_mem,
				input->rowbytes / pixelSize,
				output->rowbytes / pixelSize,
				output->width,
				output->height,
				CDL_red_slope->u.fs_d.value,
				CDL_green_slope->u.fs_d.value,
				CDL_blue_slope->u.fs_d.value,
				CDL_red_offset->u.fs_d.value,
				CDL_green_offset->u.fs_d.value,
				CDL_blue_offset->u.fs_d.value,
				CDL_red_power->u.fs_d.value,
				CDL_green_power->u.fs_d.value,
				CDL_blue_power->u.fs_d.value,
				CDL_saturation->u.fs_d.value,
				CDL_inverse->u.bd.value);

	if(cudaPeekAtLastError() != cudaSuccess)
		err = PF_Err_INTERNAL_STRUCT_DAMAGED;

	return err;
}
