
// Save_CDL.jsx
// by Brendan Bolles
//
// Write your After Effects CDL plug-ins settings as .cc, .ccc, .cdl files.
//
// Warranty: None Whatsoever
// License: Public Domain

(function(thisObj){

function SaveCDLFile(cdlFile, cdlEffects)
{
	function fileExtension(fileName)
	{
		var dotPos = fileName.lastIndexOf(".");
	
		if(dotPos < 1 || dotPos >= (fileName.length - 1))
			return null;
		
		return fileName.substr(dotPos + 1);
	}
	
	function rgbNode(effect, name)
	{
		var rVal = effect.property("Red " + name).value.toFixed(2);
		var gVal = effect.property("Green " + name).value.toFixed(2);
		var bVal = effect.property("Blue " + name).value.toFixed(2);
	
		return new XML("<" + name + ">" + rVal + " " + gVal + " " + bVal + "</" + name + ">");
	}
	
	function saturationNode(effect)
	{
		return new XML("<Saturation>" + effect.property("Saturation").value.toFixed(2) + "</Saturation>");
	}

	var rootXML = null;

	if(cdlEffects.length > 1 || fileExtension(cdlFile.displayName) == "ccc")
		rootXML = new XML("<ColorCorrectionCollection/>");
	
	for(var i=0; i < cdlEffects.length; i++)
	{
		var cdlEffect = cdlEffects[i];
	
		var ccNode = new XML("<ColorCorrection/>");
		
		if(rootXML == null)
			rootXML = ccNode;
		else
			rootXML.appendChild(ccNode);
		
		var regex = /(CDL ?[0-9]*)/; 
		
		if(cdlEffect.name.search(regex) != 0 || cdlEffect.name.replace(regex, "") != "")
			ccNode.@id = cdlEffect.name;
		
		var sopNode = new XML("<SOPNode/>");
		
		ccNode.appendChild(sopNode);
		
		sopNode.appendChild(rgbNode(cdlEffect, "Slope"));
		sopNode.appendChild(rgbNode(cdlEffect, "Power"));
		sopNode.appendChild(rgbNode(cdlEffect, "Offset"));
		
		var satNode = new XML("<SatNode/>");
		
		ccNode.appendChild(satNode);
		
		satNode.appendChild(saturationNode(cdlEffect));
	}
	
	var opened = cdlFile.open("w");
	
	if(opened)
	{
		cdlFile.encoding = "UTF-8";
	
		cdlFile.write(rootXML.toXMLString());
		
		cdlFile.close();
	}
	else
		alert("Failed to open file for writing");
}

function SaveCDL()
{
	try
	{
		if(!(app.project.activeItem instanceof CompItem) || app.project.activeItem.selectedLayers.length != 1)
		{
			alert("Select a layer with CDL effects");
			return;
		}
	
		var layer = app.project.activeItem.selectedLayers[0];
	
		if(!(layer instanceof AVLayer || layer instanceof TextLayer || layer instanceof ShapeLayer))
		{
			alert("Select a layer with CDL effects");
			return;
		}
	
		var cdlEffects = [];
	
		for(var i=1; i <= layer.effect.numProperties; i++)
		{
			var effect = layer.effect.property(i);
		
			if(effect.matchName == "fnord CDL")
				cdlEffects.push(effect);
		}
	
		if(cdlEffects.length == 0)
		{
			alert("Select a layer with CDL effects");
			return;
		}
	
		var selectedEffects = [];
	
		for(var i=0; i < cdlEffects.length; i++)
		{
			if(cdlEffects[i].selected)
				selectedEffects.push(cdlEffects[i]);
		}
	
		if(selectedEffects.length > 0)
			cdlEffects = selectedEffects;
		
		var cdlFile = File.saveDialog("Save a CDL (.cc, .ccc, .cdl)");
	
		if(cdlFile != null)
		{
			SaveCDLFile(cdlFile, cdlEffects);
		}
	}
	catch(e)
	{
		alert(e.toString());
	}
}

function buildUI(obj)
{
	var panel = (obj instanceof Panel) ? obj : new Window("palette", "Save CDL", undefined, {resizeable: false});
	
	var res =
	"group { \
		orientation: 'column', alignment: ['fill', 'fill'], \
		button: Button { text: 'Save CDL...', alignment: ['fill', 'top'] } \
	} \
	";
	
	var uiGroup = panel.add(res);
	
	uiGroup.button.onClick =
		function()
		{
			SaveCDL();
		}
	
	
	panel.layout.layout(true);
	panel.layout.resize();
	panel.onResizing = panel.onResize = function(){ panel.layout.resize(); };
	
	if(panel instanceof Window)
	{
		panel.center();
		panel.show();
	}
}

if(thisObj instanceof Panel)
	buildUI(thisObj);
else
	SaveCDL();

})(this);
