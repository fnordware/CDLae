///////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2024, Brendan Bolles
// 
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
// *	   Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
// *	   Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
///////////////////////////////////////////////////////////////////////////

// ------------------------------------------------------------------------
//
// CDL_AE_CUDA_Kernel.cu
//
// After Effects plug-in to apply a CDL
//
// ------------------------------------------------------------------------

__device__ float Power(
	float in,
	float exp)
{
	return in < 0.f ? in : pow(in, exp);
}

__device__ float CDL(
	float in,
	float slope,
	float offset,
	float power)
{
	return Power((in * slope) + offset, power);
}

__device__ float InverseCDL(
	float in,
	float slope,
	float offset,
	float power)
{
	return (Power(in, 1.f / (power == 0.f ? 1.f : power)) - offset) / (slope == 0.f ? 1.f : slope);
}

__device__ float4 Saturation(
	float4 in,
	float sat)
{
	float luma = (in.z * 0.2126f) + (in.y * 0.7152f) + (in.x * 0.0722f);

	float4 out;

	out.x = luma + sat * (in.x - luma);
	out.y = luma + sat * (in.y - luma);
	out.z = luma + sat * (in.z - luma);
	out.w = in.w;

	return out;
}

__global__ void CDL_CUDA_Kernel(
	float4 const	*input,
	float4			*output,
	int				inputPitch,
	int				outputPitch,
	unsigned int	width,
	unsigned int	height,
	float			red_slope,
	float			green_slope,
	float			blue_slope,
	float			red_offset,
	float			green_offset,
	float			blue_offset,
	float			red_power,
	float			green_power,
	float			blue_power,
	float			saturation,
	bool			inverse)
{
	uint2 pos = make_uint2(blockIdx.x * blockDim.x + threadIdx.x, blockIdx.y * blockDim.y + threadIdx.y);

	if(pos.x < width && pos.y < height)
	{
		float4 pixel = input[pos.y * inputPitch + pos.x];

		if(inverse)
		{
			if(saturation != 1.f && saturation != 0.f)
			{
				pixel = Saturation(pixel, 1.f / saturation);
			}

			pixel.x = InverseCDL(pixel.x, blue_slope, blue_offset, blue_power);
			pixel.y = InverseCDL(pixel.y, green_slope, green_offset, green_power);
			pixel.z = InverseCDL(pixel.z, red_slope, red_offset, red_power);
		}
		else
		{
			pixel.x = CDL(pixel.x, blue_slope, blue_offset, blue_power);
			pixel.y = CDL(pixel.y, green_slope, green_offset, green_power);
			pixel.z = CDL(pixel.z, red_slope, red_offset, red_power);

			if(saturation != 1.f)
			{
				pixel = Saturation(pixel, saturation);
			}
		}

		output[pos.y * outputPitch + pos.x] = pixel;
	}
}


void CDL_CUDA(
	float const		*input,
	float			*output,
	int				inputPitch,
	int				outputPitch,
	unsigned int	width,
	unsigned int	height,
	float			red_slope,
	float			green_slope,
	float			blue_slope,
	float			red_offset,
	float			green_offset,
	float			blue_offset,
	float			red_power,
	float			green_power,
	float			blue_power,
	float			saturation,
	bool			inverse)
{
	dim3 blockDim(16, 16);
	dim3 gridDim((width + blockDim.x - 1) / blockDim.x, (height + blockDim.y - 1) / blockDim.y);

	CDL_CUDA_Kernel<<<gridDim, blockDim>>>((float4 const*)input,
											(float4*)output,
											inputPitch,
											outputPitch,
											width, height,
											red_slope,
											green_slope,
											blue_slope,
											red_offset,
											green_offset,
											blue_offset,
											red_power,
											green_power,
											blue_power,
											saturation,
											inverse);

	cudaDeviceSynchronize();
}
