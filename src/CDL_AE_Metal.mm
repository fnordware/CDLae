///////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2024, Brendan Bolles
// 
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
// *	   Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
// *	   Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
///////////////////////////////////////////////////////////////////////////

// ------------------------------------------------------------------------
//
// CDL_AE_Metal.mm
//
// After Effects plug-in to apply a CDL
//
// ------------------------------------------------------------------------


#include "CDL_AE.h"

#ifndef HAVE_METAL
	#error "Something's wrong!"
#endif

#include <AE_EffectSuites.h>
#include <AE_EffectCBSuites.h>
#include <AE_EffectGPUSuites.h>

#include "AEFX_SuiteHelper.h"

#include <Metal/Metal.h>

typedef struct {
	int				inputPitch;
	int				outputPitch;
	unsigned int	width;
	unsigned int	height;
	float			red_slope;
	float			green_slope;
	float			blue_slope;
	float			red_offset;
	float			green_offset;
	float			blue_offset;
	float			red_power;
	float			green_power;
	float			blue_power;
	float			saturation;
	bool			inverse;
} CDLparams;


static NSString *cdlFunctionName = @"CDLKernel";

static const char *cdlShader = R"fnord(

using namespace metal;

float Power(
	float in,
	float exp)
{
	return in < 0.f ? in : pow(in, exp);
}

float CDL(
	float in,
	float slope,
	float offset,
	float power)
{
	return Power((in * slope) + offset, power);
}

float InverseCDL(
	float in,
	float slope,
	float offset,
	float power)
{
	return (Power(in, 1.f / (power == 0.f ? 1.f : power)) - offset) / (slope == 0.f ? 1.f : slope);
}

float4 Saturation(
	float4 in,
	float sat)
{
	float luma = (in.z * 0.2126f) + (in.y * 0.7152f) + (in.x * 0.0722f);
	
	float4 out;
	
	out.x = luma + sat * (in.x - luma);
	out.y = luma + sat * (in.y - luma);
	out.z = luma + sat * (in.z - luma);
	out.w = in.w;
	
	return out;
}

typedef struct {
	int				inputPitch;
	int				outputPitch;
	unsigned int	width;
	unsigned int	height;
	float			red_slope;
	float			green_slope;
	float			blue_slope;
	float			red_offset;
	float			green_offset;
	float			blue_offset;
	float			red_power;
	float			green_power;
	float			blue_power;
	float			saturation;
	bool			inverse;
} CDLparams;

void CDLKernel2(
	const device float4 *input,
	device float4 	*output,
	int				inputPitch,
	int				outputPitch,
	unsigned int	width,
	unsigned int	height,
	float			red_slope,
	float			green_slope,
	float			blue_slope,
	float			red_offset,
	float			green_offset,
	float			blue_offset,
	float			red_power,
	float			green_power,
	float			blue_power,
	float			saturation,
	bool			inverse,
	uint2			pos)
{
	if(pos.x < width && pos.y < height)
	{
		float4 pixel = input[(pos.y * inputPitch) + pos.x];
		
		if(inverse)
		{
			if(saturation != 1.f && saturation != 0.f)
			{
				pixel = Saturation(pixel, 1.f / saturation);
			}
			
			pixel.x = InverseCDL(pixel.x, blue_slope, blue_offset, blue_power);
			pixel.y = InverseCDL(pixel.y, green_slope, green_offset, green_power);
			pixel.z = InverseCDL(pixel.z, red_slope, red_offset, red_power);
		}
		else
		{
			pixel.x = CDL(pixel.x, blue_slope, blue_offset, blue_power);
			pixel.y = CDL(pixel.y, green_slope, green_offset, green_power);
			pixel.z = CDL(pixel.z, red_slope, red_offset, red_power);
			
			if(saturation != 1.f)
			{
				pixel = Saturation(pixel, saturation);
			}
		}
		
		output[(pos.y * outputPitch) + pos.x] = pixel;
	}
}

kernel void CDLKernel(
	const device float4* input,
	device float4* output,
	device CDLparams *params,
	uint2 pos [[ thread_position_in_grid ]])
{
	CDLKernel2(input,
				output,
				params->inputPitch,
				params->outputPitch,
				params->width,
				params->height,
				params->red_slope,
				params->green_slope,
				params->blue_slope,
				params->red_offset,
				params->green_offset,
				params->blue_offset,
				params->red_power,
				params->green_power,
				params->blue_power,
				params->saturation,
				params->inverse,
				pos);
}

)fnord";


typedef struct
{
	id<MTLComputePipelineState> cdl_pipeline;
} MetalGPUData;


PF_Err
GPUDeviceSetup_Metal(
	PF_InData	*in_data,
	PF_OutData	*out_data,
	PF_GPUDeviceSetupExtra	*extra)
{
	PF_Err err = PF_Err_NONE;
	
	AEFX_SuiteScoper<PF_HandleSuite1> handleSuite(in_data, kPFHandleSuite, kPFHandleSuiteVersion1, out_data);
	AEFX_SuiteScoper<PF_GPUDeviceSuite1> gpuDeviceSuite(in_data, kPFGPUDeviceSuite, kPFGPUDeviceSuiteVersion1, out_data);
	
	PF_GPUDeviceInfo device_info;
	gpuDeviceSuite->GetDeviceInfo(in_data->effect_ref, extra->input->device_index, &device_info);
	
	
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
	id<MTLDevice> device = (id<MTLDevice>)device_info.devicePV;
	
	NSString *cdlShaderString = [NSString stringWithUTF8String:cdlShader];
	
	NSError *error = nil;
	id<MTLLibrary> library = [[device newLibraryWithSource:cdlShaderString options:nil error:&error] autorelease];
	
	if(library != nil)
	{
		assert(error == nil); // not even a warning
	
		PF_Handle metal_handle = handleSuite->host_new_handle(sizeof(MetalGPUData));
		MetalGPUData *metal_data = (MetalGPUData *)handleSuite->host_lock_handle(metal_handle);
		
		id<MTLFunction> cdl_function = [[library newFunctionWithName:cdlFunctionName] autorelease];
		
		if(cdl_function != nil)
		{
			metal_data->cdl_pipeline = [device newComputePipelineStateWithFunction:cdl_function error:&error];
			
			if(metal_data->cdl_pipeline == nil)
				err = PF_Err_INTERNAL_STRUCT_DAMAGED;
		}
		else
			err = PF_Err_INTERNAL_STRUCT_DAMAGED;
			
		if(err == PF_Err_NONE)
		{
			extra->output->gpu_data = metal_handle;
			
			out_data->out_flags2 |= PF_OutFlag2_SUPPORTS_GPU_RENDER_F32;
		}
		
		handleSuite->host_unlock_handle(metal_handle);
	}
	else
		err = PF_Err_INTERNAL_STRUCT_DAMAGED;

	[pool release];

	return err;
}


PF_Err
GPUDeviceSetdown_Metal(
	PF_InData	*in_data,
	PF_OutData	*out_data,
	PF_GPUDeviceSetdownExtra	*extra)
{
	PF_Err err = PF_Err_NONE;
	
	if(extra->input->gpu_data != NULL)
	{
		AEFX_SuiteScoper<PF_HandleSuite1> handleSuite(in_data, kPFHandleSuite, kPFHandleSuiteVersion1, out_data);
		
		handleSuite->host_dispose_handle((PF_Handle)extra->input->gpu_data);
	}
	else
		assert(FALSE);
	
	return err;
}


PF_Err
GPURender_Metal(
	PF_InData		*in_data,
	PF_EffectWorld	*input,
	PF_ParamDef		*CDL_red_slope,
	PF_ParamDef		*CDL_green_slope,
	PF_ParamDef		*CDL_blue_slope,
	PF_ParamDef		*CDL_red_offset,
	PF_ParamDef		*CDL_green_offset,
	PF_ParamDef		*CDL_blue_offset,
	PF_ParamDef		*CDL_red_power,
	PF_ParamDef		*CDL_green_power,
	PF_ParamDef		*CDL_blue_power,
	PF_ParamDef		*CDL_saturation,
	PF_ParamDef		*CDL_inverse,
	PF_OutData		*out_data,
	PF_EffectWorld	*output,
	PF_SmartRenderExtra	*extraP)
{
	PF_Err err = PF_Err_NONE;
	
#ifndef NDEBUG
	AEFX_SuiteScoper<PF_WorldSuite2> worldSuite(in_data, kPFWorldSuite, kPFWorldSuiteVersion2, out_data);
	
	PF_PixelFormat inputFormat, outputFormat;
	worldSuite->PF_GetPixelFormat(input, &inputFormat);
	worldSuite->PF_GetPixelFormat(output, &outputFormat);
	assert(inputFormat == PF_PixelFormat_GPU_BGRA128);
	assert(outputFormat == PF_PixelFormat_GPU_BGRA128);
	
	assert(input->width == output->width);
	assert(input->height == output->height);
#endif
	
	AEFX_SuiteScoper<PF_HandleSuite1> handleSuite(in_data, kPFHandleSuite, kPFHandleSuiteVersion1, out_data);
	AEFX_SuiteScoper<PF_GPUDeviceSuite1> gpuDeviceSuite(in_data, kPFGPUDeviceSuite, kPFGPUDeviceSuiteVersion1, out_data);
	
	PF_GPUDeviceInfo device_info;
	gpuDeviceSuite->GetDeviceInfo(in_data->effect_ref, extraP->input->device_index, &device_info);
	
	void *input_mem = NULL;
	gpuDeviceSuite->GetGPUWorldData(in_data->effect_ref, input, &input_mem);
	
	void *output_mem = NULL;
	gpuDeviceSuite->GetGPUWorldData(in_data->effect_ref, output, &output_mem);
	
	MetalGPUData *metal_data = (MetalGPUData *)handleSuite->host_lock_handle((Handle)extraP->input->gpu_data);
	
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
	id<MTLDevice> device = (id<MTLDevice>)device_info.devicePV;
	
	const size_t pixelSize = 4 * sizeof(float);
	
	CDLparams params;
	
	params.inputPitch = input->rowbytes / pixelSize;
	params.outputPitch = output->rowbytes / pixelSize;
	params.width = output->width;
	params.height = output->height;
	params.red_slope = CDL_red_slope->u.fs_d.value;
	params.green_slope = CDL_green_slope->u.fs_d.value;
	params.blue_slope = CDL_blue_slope->u.fs_d.value;
	params.red_offset = CDL_red_offset->u.fs_d.value;
	params.green_offset = CDL_green_offset->u.fs_d.value;
	params.blue_offset = CDL_blue_offset->u.fs_d.value;
	params.red_power = CDL_red_power->u.fs_d.value;
	params.green_power = CDL_green_power->u.fs_d.value;
	params.blue_power = CDL_blue_power->u.fs_d.value;
	params.saturation = CDL_saturation->u.fs_d.value;
	params.inverse = CDL_inverse->u.bd.value;
	
	id<MTLBuffer> cdl_params_buffer = [[device newBufferWithBytes:&params length:sizeof(CDLparams) options:MTLResourceStorageModeManaged] autorelease];
	
	id<MTLCommandQueue> queue = (id<MTLCommandQueue>)device_info.command_queuePV;
	id<MTLCommandBuffer> commandBuffer = [queue commandBuffer];
	id<MTLComputeCommandEncoder> computeEncoder = [commandBuffer computeCommandEncoder];
	
	id<MTLBuffer> input_metal_buffer = (id<MTLBuffer>)input_mem;
	id<MTLBuffer> output_metal_buffer = (id<MTLBuffer>)output_mem;
	
	MTLSize threadsPerGroup = {[metal_data->cdl_pipeline threadExecutionWidth], 16, 1};
	MTLSize numThreadgroups = { (NSUInteger)ceilf((float)input->width / (float)threadsPerGroup.width), (NSUInteger)ceilf((float)input->height / (float)threadsPerGroup.height), 1};
	
	[computeEncoder setComputePipelineState:metal_data->cdl_pipeline];
	[computeEncoder setBuffer:input_metal_buffer offset:0 atIndex:0];
	[computeEncoder setBuffer:output_metal_buffer offset:0 atIndex:1];
	[computeEncoder setBuffer:cdl_params_buffer offset:0 atIndex:2];
	[computeEncoder dispatchThreadgroups:numThreadgroups threadsPerThreadgroup:threadsPerGroup];
	[computeEncoder endEncoding];
	[commandBuffer commit];
	
	if([commandBuffer error] != nil)
		err = PF_Err_INTERNAL_STRUCT_DAMAGED;
	
	[pool release];
	
	handleSuite->host_unlock_handle((Handle)extraP->input->gpu_data);
	
	return err;
}
